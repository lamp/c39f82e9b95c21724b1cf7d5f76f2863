# How to safely share your VPS


Suppose you have a [nice VPS](https://contabo.com/?show=vps) but you're only using a fraction of its resources and you want to share the surplus with friends who need to host stuff, but don't want them to have any access to your stuff.

Using [Linux containers](https://linuxcontainers.org/), you can create a limited isolated environment that's just like a VPS which you can give to friends to use, without letting them access any of your stuff. You can set limits on memory, CPU, disk and networking so they can't hog your resources and cause you any trouble.

To do this you can use [LXD](https://linuxcontainers.org/lxd/), an easy-to-use LXC container management daemon.

------------

Snap is the recommended way to install LXD and you probably have it.

```
snap install lxd
```

If not, install it with your native package manager (i.e. `apt`/`dnf`/`zypper` `install` `lxd`).

Then run
```
lxd init
```
Hit enter at each prompt to use the default, until you get to the size of the loop device. 5GB may be too small; you should set it to at least 20GB if you have enough free space. You can set it to a few GB less than the amount of free space you have. The disk image is dynamically-allocating so it'll only use as much space as is used inside, but it's useful to limit the maximum disk usage.

Hit enter for the defaults on the rest of the prompts.

Now you can create a container. I recommend ubuntu 18.04 because 20.04 doesn't work with xfce vnc for some reason.

```
lxc launch ubuntu:18.04
```

LXD will make up a name for you, for example, `topical-trout`; or you can specify a name as the last argument.

Now enter the container to set up some things.

```
lxc exec topical-trout -- bash
```

Ubuntu has SSH password authentication disabled so unless you can append your client's (friend's) public key into /home/ubuntu/.ssh/authorized_keys, enabling password authentication will make things easier.

```
nano /etc/ssh/sshd_config
```
Locate and change `PasswordAuthentication no` to `PasswordAuthentication yes`, then press ctrl S and ctrl X.

Run `systemctl restart sshd` for changes to take effect.

Now set a strong password or [passphrase](https://www.useapassphrase.com/) on the `ubuntu` user:
```
passwd ubuntu
```

Then `exit` the container when you're done.

Last but not least, you need to forward some ports so the container can be accessed from the internet. To forward ports, use this command:
```
lxc config device add <container> <device name> proxy connect=tcp:127.0.0.1:<container port> listen=tcp:0.0.0.0:<external port>
```

You'll need to forward the SSH port so your client can connect to the container:
```
lxc config device add topical-trout ssh proxy connect=tcp:127.0.0.1:22 listen=tcp:0.0.0.0:23232
```

Make sure to choose an external port number that isn't used by anything else.

Repeat this for any other ports your client wants open to the internet. For example, if they want to host a minceraft server:
```
lxc config device add topical-trout minecraft proxy connect=tcp:127.0.0.1:25565 listen=tcp:0.0.0.0:333
```
Then they can connect to their minecraft server on port 333.

So that's about it. Now you can give your client the credentials to connect to their container. So for example, if your VPS's address is 207.180.221.93 and you forwarded port 23232 to the container's SSH server, they can connect with `ssh 207.180.221.93 -p 23232 -l ubuntu` using the password you set on ubuntu. 

You might want to limit RAM and CPU usage as well. To limit memory:
```
lxc config set topical-trout limits.memory 4GiB
```
This will limit the container to 4 gibibytes of memory.

And to reduce the number of CPU cores:
```
lxc config set topical-trout limits.cpu 2
```
This will give it two cores. If you give it half the number of cores as your VPS has, it will only be able to use half of your CPU.



